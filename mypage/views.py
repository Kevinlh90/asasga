from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, JsonResponse
from .forms import Status_Form, Profile_Form
from .models import Status, Profile
from django.contrib.auth import logout as logout_user
from django.template import RequestContext
import json
import requests

# Create your views here.

def index(request):
	all_status = Status.objects.all()
	form = Status_Form
	return render(request, 'index.html', {'Status': all_status, 'form': form, 'nbar': 'index'})

def profile(request):
	return render(request, 'profile.html', {'nbar': 'profile'})

def logout(request):
    logout_user(request)
    return HttpResponseRedirect('/registration/')

def books(request):
    return render(request, 'books.html', {'nbar': 'books',
                                         'context_instance': RequestContext(request)})

def registration(request):
	form = Profile_Form
	return render(request, 'registration.html', {'form': form, 'nbar': 'registration'})

def validate_form(request):
    username = request.GET.get('username', None)
    email = request.GET.get('email', None)
    password = request.GET.get('password', None)
    confirm_password = request.GET.get('confirm_password', None)
    
    v_email = Profile.objects.filter(email=email).exists()
    v_password = True if (password == confirm_password) else False
    v_all = not v_email and email and username and password and confirm_password and v_password
    
    data = {
        'is_taken': v_email,
        'is_equal': v_password,
        'is_submitable': v_all
    }
    
    return JsonResponse(data)

def add_profile(request):
    print("im here")
    form = Profile_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        email = request.POST.get('email', False)
        username = request.POST.get('username', False)
        password = request.POST.get('password', False)
        confirm_password = request.POST.get('confirm_password', False)

        profile = Profile(email=email,
                          username=username,
                          password=password,
                          confirm_password=confirm_password)
        
        profile.save()
        return redirect(registration)
    else:
        return HttpResponseRedirect('/registration/')

def search_book(request, text=None):
    url = 'https://www.googleapis.com/books/v1/volumes?q='+text
    return JsonResponse(requests.get(url).json())

def add_status(request):
	form = Status_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		text = request.POST.get('text', False)
		text = Status(text=text)
		text.save()
		return redirect(index)
	else:
		return HttpResponseRedirect('/mypage:index/')
    
def favorite(request):
    book_data = json.loads(request.body.decode())
    
    added = book_data['added']
    length = request.session.get('length', 0)

    if(added == 'true'):
        request.session['length'] = length+1
    else:
        request.session['length'] = length-1
        
    return JSONResponse({'length': request.session['length']})