from django import forms

class Status_Form(forms.Form):
	error_messages = {
        'required': 'This field is required',
        'invalid': 'is not valid',
	}

	attrs = {
        'class': 'form-control'
	}
	text = forms.CharField(label='status', required=True, max_length=300,
	widget=forms.TextInput(attrs=attrs))
    
class Profile_Form(forms.Form):
    error_messages = {
        'required': 'This field is required',
        'invalid': 'is not valid',
    }

    attrs = {
        'class': 'form-input',
        'type':'text'
    }
    
    def merge_two_dicts(x, y):
        z = x.copy()   # start with x's keys and values
        z.update(y)    # modifies z with y's keys and values & returns None
        return z
    
    adi_attrs = {
        'id': 'form-email',
        'placeholder': 'Email',
        'name': 'email'
    }
    email = forms.CharField(label='email', required=True, max_length=300,
    widget=forms.TextInput(attrs=merge_two_dicts(attrs, adi_attrs)))
    
    adi_attrs = {
        'id': 'form-username',
        'placeholder': 'Username',
        'name': 'username'
    }
    username = forms.CharField(label='username', max_length=300,
    widget=forms.TextInput(merge_two_dicts(attrs, adi_attrs)))
    
    adi_attrs = {
        'id': 'form-password',
        'placeholder': 'Password',
        'name': 'password',
        'type':'password'
    }
    password = forms.CharField(label='password', max_length=300,
    widget=forms.TextInput(attrs=merge_two_dicts(attrs, adi_attrs)))
    
    adi_attrs = {
        'id': 'form-confirm-password',
        'placeholder': 'Confirm Password',
        'name': 'confirm password',   
        'type':'password'
    }
    confirm_password = forms.CharField(label='confirm_password', max_length=300,
    widget=forms.TextInput(attrs=merge_two_dicts(attrs, adi_attrs)))